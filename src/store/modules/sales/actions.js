export default {
  async loadSales(context, data) {
    const response = await fetch('http://localhost/sales-test-lefort-API/Sales.php', {
      method: 'POST',
      body: JSON.stringify(data),
    });

    const salesData = await response.json();

    if (!response.ok) {
      const error = new Error(salesData.message || 'Failed to create');
      throw error;
    }

    context.commit('setSales', salesData);
  },
};
