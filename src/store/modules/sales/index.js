import mutations from './mutations';
import actions from './actions';
import getters from './getters';

export default {
  namespaced: true,
  state() {
    return {
      sales: [{
        id: '00', customer_id: 'AA', date: '2021-12-13 03:20:00', total: 5000,
      }],
    };
  },
  mutations,
  actions,
  getters,
};
