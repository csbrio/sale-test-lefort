import { createStore } from 'vuex';
import salesModule from './modules/sales/index';

export default createStore({
  modules: {
    sales: salesModule,
  },
  state: {
  },
  mutations: {},
  actions: {},
});
